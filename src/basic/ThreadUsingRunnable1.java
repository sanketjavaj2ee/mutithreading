
class Worker1 implements Runnable {

	private boolean isTerminated = false;

	@Override
	public void run() {

		while (!isTerminated) {

			System.out.println("Hello from worker class...");

			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public boolean isTerminated() {
		return isTerminated;
	}

	public void setTerminated(boolean isTerminated) {
		this.isTerminated = isTerminated;
	}
}

public class ThreadUsingRunnable1 {

	public static void main(String[] args) {

		Worker1 worker = new Worker1();
		Thread t1 = new Thread(worker);
		t1.start();

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		worker.setTerminated(true);
		System.out.println("Finished...");
	}
}
