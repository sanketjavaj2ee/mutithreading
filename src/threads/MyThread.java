
public class MyThread extends Thread {
	String threadName;
	
	public MyThread(String threadName){
		this.threadName = threadName;
	}

	public void run(){
		try{
			Thread.sleep(1000);
			System.out.println(threadName + " is running" );
		}catch(InterruptedException e){
			System.out.println(e.getMessage());
		}
	}
	
	public static void main(String[] args){
		Thread t = new Thread(new MyThread("firstThread"));
		Thread t1 = new Thread(new MyThread("secondThread"));
		t.start();
		t1.start();
		
	}
}
