public class ThreadSupportClass {

	public static void main(String[] args){
		Counter c = new Counter();
		Thread threadA = new CounterThread(c);
		Thread threadB = new CounterThread(c);
		threadA.start();
		threadB.start();
	}
	
	
}
