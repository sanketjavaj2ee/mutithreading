package writeLog;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;
import java.util.concurrent.TimeUnit;
/* Task that writes all the messages in the log file every ten seconds.
 * It implements the Runnable interface. It will be executed as a thread
 * @author author
 *
 */

public class LogTask implements Runnable {

	@Override
	public void run() {
		try {
			while (true) {
				TimeUnit.SECONDS.sleep(10);
				// Logger.writeLogs();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method that write all the messages in the queue to the file
	 */
/*	public static void writeLogs() {
		String message;
		Path path = Paths.get(ROUTE);
		try (BufferedWriter fileWriter = Files.newBufferedWriter(path, StandardOpenOption.CREATE,
				StandardOpenOption.APPEND)) {
			while ((message = logQueue.poll()) != null) {
				StringWriter writer = new StringWriter();
				writer.write(new Date().toString());
				writer.write(": ");
				writer.write(message);
				fileWriter.write(writer.toString());
				fileWriter.newLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
*/
}
