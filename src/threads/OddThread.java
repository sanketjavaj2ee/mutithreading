
public class OddThread extends Thread
{

	Printer printer;
	
    public OddThread(Printer printer) {
    	 this.printer = printer;
         new Thread( this, "EvenThread" ).start();
	}

	public static void main( String[] args )
    {
		EvenThread evenThread = new EvenThread(new Printer());
        
        synchronized ( evenThread )
        {
            for(int i=1; i<100; i=i+2 )
            {
                System.out.println(i);
                //notify();
            }
        }
        
    }

}
