package service;

class ProducerConsumer {
	public static void main(String args[]) {
		Queue q = new Queue();
		new Producer(q);
		new Consumer(q);
		System.out.println("Press Control-C to stop.");
	}
}

class Consumer implements Runnable {
	Queue q;

	Consumer(Queue q) {
		this.q = q;
		new Thread(this, "Consumer").start();
	}

	public void run() {
		while (true) {
			q.get();
		}
	}
}

class Producer implements Runnable {
	Queue q;

	Producer(Queue q) {
		this.q = q;
		new Thread(this, "Producer").start();
	}

	public void run() {
		int i = 0;
		while (true) {
			q.put(i++);
		}
	}
}
