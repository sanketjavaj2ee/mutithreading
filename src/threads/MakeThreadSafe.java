
public class MakeThreadSafe implements Runnable {

	NotThreadSafe instance;
	public MakeThreadSafe(NotThreadSafe instance){
		this.instance = instance;
	}
	
	public void run(){
		this.instance.add("some text");
	}
	
	public static void main(String[] args){
		NotThreadSafe instance = new NotThreadSafe();
		new Thread(new MakeThreadSafe(instance)).start();
		new Thread(new MakeThreadSafe(instance)).start();
	}
}
