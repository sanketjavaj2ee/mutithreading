
public class MyRunnable implements Runnable{

	String name;
	public MyRunnable(String name){
		this.name = name;
	}
	
	public void run(){
		try{
			Thread.sleep(1000);
			System.out.println(name + " is running.");
		}catch(InterruptedException e){
			System.out.println(e.getMessage());
		}
	}
	
	public static void main(String[] args){
		MyRunnable runnable = new MyRunnable("firstThread");
		MyRunnable runnable1 = new MyRunnable("secondThread");
		Thread thread = new Thread(runnable);
		Thread thread1 = new Thread(runnable1);
		thread.start();
		thread1.start();
	}
}
