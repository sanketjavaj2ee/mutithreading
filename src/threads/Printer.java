
public class Printer
{
    private static int odd = 1;
    private static int even = 2;
    boolean valueSet = false;
    
    synchronized void odd()
    {
        if ( valueSet )
        {
            try
            {
                System.out.println("In ODD wait");
                wait();
            }
            catch ( InterruptedException e )
            {
                System.out.println( "InterruptedException caught" );
            }
        }
        
        valueSet = true;
        System.out.println(odd);
        odd = odd + 2;
        notify();
    }
    
    synchronized void even()
    {
        if ( !valueSet )
        {
            try
            {
                System.out.println("In Even wait");
                wait();
            }
            catch ( InterruptedException e )
            {
                System.out.println( "InterruptedException caught" );
            }
        }
        
        System.out.println(even);
        even = even + 2;
        valueSet = false;
        notify();
    }
}
