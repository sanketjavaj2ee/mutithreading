
public class EvenThread implements Runnable
{
    Printer printer;

    EvenThread( Printer printer )
    {
        this.printer = printer;
        new Thread( this, "EvenThread" ).start();
    }

    public void run()
    {
        int i = 0;
        while ( i < 50 )
        {
            printer.even( );
            i++;
        }
    }
}
