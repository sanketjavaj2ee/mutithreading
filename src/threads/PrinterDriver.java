
public class PrinterDriver
{
    public static void main( String[] args )
    {
        Printer printer = new Printer();
        new OddThread( printer );
        new EvenThread( printer );
        System.out.println( "Press Control-C to stop." );
    }

}
