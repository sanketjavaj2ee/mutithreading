package multithreading.synchronization;

public class ThreadTest {

	public static void main(String[] args) {
		Common c = new Common();
		Thread t1 = new MyThread("Thread-1", 0, c);
		Thread t2 = new MyThread("Thread-2", 1, c);
		t1.start();
		t2.start();
	}
}
