package multithreading.synchronization;

public class MyThread extends Thread {

	private int id = 0;
	private Common common;
	
	public MyThread(String name, int no, Common object){
		super(name);
		id = no;
		common = object;
	}
	
	public void run(){
		System.out.println("Running thread "+this.getName());
		if(id == 0){
			common.synchronizedMethod1();
		}else{
			common.synchronizedMethod2();
			//common.method1();
		}
	}
}
