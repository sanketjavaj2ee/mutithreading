package multithreading.synchronization;

public class Common {

	public synchronized void synchronizedMethod1(){
		System.out.println("Synchronized method1 called");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException e){
			e.printStackTrace();
		}
		System.out.println("Synchronization method1 done");
	}
	
	public synchronized void synchronizedMethod2(){
		System.out.println("Synchronized method2 called");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException e){
			e.printStackTrace();
		}
		System.out.println("Synchronization method2 done");
	}
	
	public void method1(){
		System.out.println("method1 is called");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException e){
			e.printStackTrace();
		}
		System.out.println("method1 is done");
	}
}
